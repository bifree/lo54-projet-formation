class ClientScript {
    boolean subscribeToCourseSession(clientId, courseSessionId, clientRepository, courseSessionService) {
        if (clientId == null || courseSessionId == null || clientRepository == null || courseSessionService == null)
            return false

        // get the client
        def client = clientRepository.getById(clientId)

        // get the courseSession
        def courseSession = courseSessionService.getCourseSessionById(courseSessionId)

        // replace client's courseSessionId by the one given in parameters
        client.courseSession = courseSession

        // save client with subscription to courseSession
        return clientRepository.update(client)
    }
}





