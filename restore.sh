#!/bin/bash

# exit the script if error
set -e

# get the password
if [ "$1" != "" ]; then
    PGPASSWORD = $1
else
    echo "Password: "
    read PGPASSWORD
fi

# drop the database
PGPASSWORD=$PGPASSWORD C:/Program\ Files/PostgreSQL/12/bin/dropdb -U postgres --if-exists FORMATIONS;

# create the database used for the restoration
PGPASSWORD=$PGPASSWORD C:/Program\ Files/PostgreSQL/12/bin/createdb -U postgres -O postgres FORMATIONS;

# restore our data
PGPASSWORD=$PGPASSWORD C:/Program\ Files/PostgreSQL/12/bin/psql -U postgres -d FORMATIONS -f ./backup.sql

# display the result
if [ $? -eq 0 ]; then
    echo "Success"
else
    echo "Restore failed !"
fi