--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public."COURSE_SESSION" DROP CONSTRAINT "FK_LOCATION_ID";
ALTER TABLE ONLY public."CLIENT" DROP CONSTRAINT "FK_COURSE_SESSION_ID";
ALTER TABLE ONLY public."COURSE_SESSION" DROP CONSTRAINT "FK_COURSE_CODE";
ALTER TABLE ONLY public."LOCATION" DROP CONSTRAINT "LOCATION_pkey";
ALTER TABLE public."COURSE_SESSION" DROP CONSTRAINT "DATE_COHERENCE";
ALTER TABLE ONLY public."COURSE" DROP CONSTRAINT "COURSE_pkey";
ALTER TABLE ONLY public."COURSE_SESSION" DROP CONSTRAINT "COURSE_SESSION_pkey";
ALTER TABLE ONLY public."CLIENT" DROP CONSTRAINT "CLIENT_pkey";
ALTER TABLE public."LOCATION" ALTER COLUMN "LOCATION_ID" DROP DEFAULT;
ALTER TABLE public."COURSE_SESSION" ALTER COLUMN "COURSE_SESSION_ID" DROP DEFAULT;
ALTER TABLE public."CLIENT" ALTER COLUMN "CLIENTID" DROP DEFAULT;
DROP SEQUENCE public."LOCATION_LOCATION_ID_seq";
DROP TABLE public."LOCATION";
DROP SEQUENCE public."COURSE_SESSION_COURSE_SESSION_ID_seq";
DROP TABLE public."COURSE_SESSION";
DROP TABLE public."COURSE";
DROP SEQUENCE public."CLIENT_ID_seq";
DROP TABLE public."CLIENT";
SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: CLIENT; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."CLIENT" (
    "COURSE_SESSION_ID" integer NOT NULL,
    "CLIENTID" integer NOT NULL,
    "LASTNAME" character varying(20) NOT NULL,
    "FIRSTNAME" character varying(20) NOT NULL,
    "ADDRESS_CITY" character varying(20),
    "ADDRESS_STREET" character varying(70),
    "PHONE" character varying(15) NOT NULL,
    "EMAIL" character varying(40) NOT NULL
);


ALTER TABLE public."CLIENT" OWNER TO postgres;

--
-- Name: CLIENT_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."CLIENT_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CLIENT_ID_seq" OWNER TO postgres;

--
-- Name: CLIENT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."CLIENT_ID_seq" OWNED BY public."CLIENT"."CLIENTID";


--
-- Name: COURSE; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."COURSE" (
    "CODE" character varying(5) NOT NULL,
    "TITLE" character varying(60) NOT NULL
);


ALTER TABLE public."COURSE" OWNER TO postgres;

--
-- Name: COURSE_SESSION; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."COURSE_SESSION" (
    "COURSE_SESSION_ID" integer NOT NULL,
    "START_DATE" date NOT NULL,
    "END_DATE" date NOT NULL,
    "MAX" integer,
    "LOCATION_ID" integer NOT NULL,
    "COURSE_CODE" character varying(5) NOT NULL
);


ALTER TABLE public."COURSE_SESSION" OWNER TO postgres;

--
-- Name: COURSE_SESSION_COURSE_SESSION_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."COURSE_SESSION_COURSE_SESSION_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."COURSE_SESSION_COURSE_SESSION_ID_seq" OWNER TO postgres;

--
-- Name: COURSE_SESSION_COURSE_SESSION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."COURSE_SESSION_COURSE_SESSION_ID_seq" OWNED BY public."COURSE_SESSION"."COURSE_SESSION_ID";


--
-- Name: LOCATION; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."LOCATION" (
    "LOCATION_ID" integer NOT NULL,
    "CITY" character varying(20) NOT NULL
);


ALTER TABLE public."LOCATION" OWNER TO postgres;

--
-- Name: LOCATION_LOCATION_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."LOCATION_LOCATION_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."LOCATION_LOCATION_ID_seq" OWNER TO postgres;

--
-- Name: LOCATION_LOCATION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."LOCATION_LOCATION_ID_seq" OWNED BY public."LOCATION"."LOCATION_ID";


--
-- Name: CLIENT CLIENTID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CLIENT" ALTER COLUMN "CLIENTID" SET DEFAULT nextval('public."CLIENT_ID_seq"'::regclass);


--
-- Name: COURSE_SESSION COURSE_SESSION_ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."COURSE_SESSION" ALTER COLUMN "COURSE_SESSION_ID" SET DEFAULT nextval('public."COURSE_SESSION_COURSE_SESSION_ID_seq"'::regclass);


--
-- Name: LOCATION LOCATION_ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."LOCATION" ALTER COLUMN "LOCATION_ID" SET DEFAULT nextval('public."LOCATION_LOCATION_ID_seq"'::regclass);


--
-- Data for Name: CLIENT; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."CLIENT" ("COURSE_SESSION_ID", "CLIENTID", "LASTNAME", "FIRSTNAME", "ADDRESS_CITY", "ADDRESS_STREET", "PHONE", "EMAIL") FROM stdin;
\.


--
-- Data for Name: COURSE; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."COURSE" ("CODE", "TITLE") FROM stdin;
BD50	Administration de bases de données
\.


--
-- Data for Name: COURSE_SESSION; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."COURSE_SESSION" ("COURSE_SESSION_ID", "START_DATE", "END_DATE", "MAX", "LOCATION_ID", "COURSE_CODE") FROM stdin;
\.


--
-- Data for Name: LOCATION; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."LOCATION" ("LOCATION_ID", "CITY") FROM stdin;
\.


--
-- Name: CLIENT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."CLIENT_ID_seq"', 2, true);


--
-- Name: COURSE_SESSION_COURSE_SESSION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."COURSE_SESSION_COURSE_SESSION_ID_seq"', 1, false);


--
-- Name: LOCATION_LOCATION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."LOCATION_LOCATION_ID_seq"', 1, false);


--
-- Name: CLIENT CLIENT_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CLIENT"
    ADD CONSTRAINT "CLIENT_pkey" PRIMARY KEY ("CLIENTID");


--
-- Name: COURSE_SESSION COURSE_SESSION_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."COURSE_SESSION"
    ADD CONSTRAINT "COURSE_SESSION_pkey" PRIMARY KEY ("COURSE_SESSION_ID");


--
-- Name: COURSE COURSE_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."COURSE"
    ADD CONSTRAINT "COURSE_pkey" PRIMARY KEY ("CODE");


--
-- Name: COURSE_SESSION DATE_COHERENCE; Type: CHECK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE public."COURSE_SESSION"
    ADD CONSTRAINT "DATE_COHERENCE" CHECK (("START_DATE" < "END_DATE")) NOT VALID;


--
-- Name: CONSTRAINT "DATE_COHERENCE" ON "COURSE_SESSION"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT "DATE_COHERENCE" ON public."COURSE_SESSION" IS 'Verify the end date of the session is after the start date ';


--
-- Name: LOCATION LOCATION_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."LOCATION"
    ADD CONSTRAINT "LOCATION_pkey" PRIMARY KEY ("LOCATION_ID");


--
-- Name: COURSE_SESSION FK_COURSE_CODE; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."COURSE_SESSION"
    ADD CONSTRAINT "FK_COURSE_CODE" FOREIGN KEY ("COURSE_CODE") REFERENCES public."COURSE"("CODE") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CLIENT FK_COURSE_SESSION_ID; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CLIENT"
    ADD CONSTRAINT "FK_COURSE_SESSION_ID" FOREIGN KEY ("COURSE_SESSION_ID") REFERENCES public."COURSE_SESSION"("COURSE_SESSION_ID") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: COURSE_SESSION FK_LOCATION_ID; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."COURSE_SESSION"
    ADD CONSTRAINT "FK_LOCATION_ID" FOREIGN KEY ("LOCATION_ID") REFERENCES public."LOCATION"("LOCATION_ID") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SEQUENCE "CLIENT_ID_seq"; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE public."CLIENT_ID_seq" FROM postgres;
GRANT ALL ON SEQUENCE public."CLIENT_ID_seq" TO postgres WITH GRANT OPTION;


--
-- PostgreSQL database dump complete
--

