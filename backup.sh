#!/bin/bash

# get the password
if [ "$1" != "" ]; then
    PGPASSWORD = $1
else
    echo "Password: "
    read PGPASSWORD
fi

PGPASSWORD=$PGPASSWORD C:/Program\ Files/PostgreSQL/12/bin/pg_dump -c -U postgres FORMATIONS > ./dump/backup_`date +%d-%m-%Y"_"%H_%M_%S`.sql
PGPASSWORD=$PGPASSWORD C:/Program\ Files/PostgreSQL/12/bin/pg_dump -c -U postgres FORMATIONS > ./backup.sql

# display the result
if [ $? -eq 0 ]; then
    echo "Success"
else
    echo "Backup failed !"
fi