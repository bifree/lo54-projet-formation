package fr.utbm.lo54.formations.services;

import fr.utbm.lo54.formations.entities.Course;
import fr.utbm.lo54.formations.entities.CourseSession;
import fr.utbm.lo54.formations.repositories.CourseRepository;

import java.util.*;

/**
 * Singleton class
 */
public class CourseService {
    private final CourseRepository courseRepository = CourseRepository.getInstance();
    private static CourseService instance;

    private CourseService() {}

    public static CourseService getInstance() {
        if (instance == null)
            instance = new CourseService();

        return instance;
    }

    /**
     * Add `course` to DB, and return it (with ID from DB).
     * @param course record to add to the DB
     * @return the newly saved course
     */
    public Course createCourse(Course course) {
        return this.courseRepository.save(course);
    }

    /**
     * Update `course` into DB, and return it.
     * @param course record to update
     * @return true if successful; false otherwise
     */
    public boolean updateCourse(Course course) {
        return this.courseRepository.update(course);
    }

    /**
     * Delete `course` from DB.
     * @param code used to identify the record to delete
     * @return true if successful; false otherwise
     */
    public boolean deleteCourse(String code) {
        return this.courseRepository.delete(code);
    }

    /**
     * Get a course thanks to its ID.
     * @param code used to identify the record to get
     * @return the course which correpond to the given ID
     */
    public Course getCourseById(String code) {
        return this.courseRepository.getById(code);
    }

    /**
     * Get all course.
     * @return the list of all the courses
     */
    public List<Course> getAllCourses() {
        return this.courseRepository.getAll();
    }

    /**
     * Get all course.
     * @return the list of all the courses
     */
    public List<Course> getAllCourses(String orderBy) {
        return this.courseRepository.getAll(orderBy);
    }

    /**
     * Get some courses according to the filters
     * @param filterTitle filter on the title of the courses
     * @param filterDate filter on the start date of the sessions of the courses
     * @param filterLocations filter on the location of the sessions of the courses
     */
    public List<Course> getSome(String filterTitle, String filterDate, String[] filterLocations) {
        return this.courseRepository.getSome(filterTitle, filterDate, filterLocations);
    }

    /**
     * Get all course sessions for a given course.
     * @param code used to identify for which course we want to get the sessions
     * @return the list of all the session for the given course
     */
    public Map<String, Object> getCourseSessionsByCourseCode(String code) {
        Course course = this.courseRepository.getById(code);
        List<CourseSession> sessions = course.getCourseSessions();

        Map<String, Object> courseAndSessions = new HashMap<>();
        courseAndSessions.put("course", course);
        courseAndSessions.put("sessions", sessions);

        return courseAndSessions;
    }

    /**
     * Add a `session` to `course`, then update `course`.
     * @param session the new session for the given course
     * @param course the course for which a session is added
     * @return true if successful; false otherwise
     */
    public boolean addSessionToCourse(CourseSession session, Course course) {
        // add the session to course
        course.addSession(session);

        // update course
        return courseRepository.update(course);
    }

    /**
     * Remove a session (identified by `sessionId`) from `course`, then update `course`.
     * @param sessionId used to identify the session to remove from the list of sessions of a given course
     * @param course the course from which the session is removed
     * @return true if successful; false otherwise
     */
    public boolean removeSessionFromCourse(Integer sessionId, Course course) {
        // remove the session from course
        course.removeSession(sessionId);

        // update course
        return courseRepository.update(course);
    }
}
