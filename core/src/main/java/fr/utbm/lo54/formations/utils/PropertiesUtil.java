package fr.utbm.lo54.formations.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    private static final Properties properties;

    static {
        // Create the stream from the config.properties file
        properties = new Properties();
        InputStream stream = PropertiesUtil.class.getClassLoader().getResourceAsStream("config.properties");
        try {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String prop) {
        String property = properties.getProperty(prop);
        return property;
    }
}
