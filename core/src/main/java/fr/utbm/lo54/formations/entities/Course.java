package fr.utbm.lo54.formations.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name="\"COURSE\"")
public class Course implements Serializable {
    @Id
    @Column(name = "\"CODE\"")
    private String codeCourse;

    @Column(name = "\"TITLE\"")
    private String title;

    @OneToMany (
            mappedBy = "course",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CourseSession> courseSessions = new ArrayList<>();

    public Course() {
    }

    public Course(String codeCourse, String title) {
        this.codeCourse = codeCourse;
        this.title = title;
    }

    public int getNumberOfSessions() {
        return this.courseSessions.size();
    }

    /**
     * Add session to `course`, if not already present.
     * @param session record to add to the list of session for this course session
     */
    public void addSession(CourseSession session) {
        if (this.courseSessions.stream()
                .noneMatch(courseSession ->
                    courseSession.getCourseSessionId().equals(session.getCourseSessionId())
                )
        )
            this.courseSessions.add(session);
    }

    /**
     * Remove from `course` the session indicated by `sessionId`.
     * @param sessionId used to identify the session to remove from this course
     */
    public void removeSession(Integer sessionId) {
        this.courseSessions = this.courseSessions.stream().filter(courseSession ->
                courseSession.getCourseSessionId().equals(sessionId)).collect(Collectors.toList()
        );
    }

    public String toString() {
        return "Course{"
                + " code=" + codeCourse
                + ", title=" + title
                + " }";
    }
}
