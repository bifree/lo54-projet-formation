package fr.utbm.lo54.formations.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "\"LOCATION\"", schema = "PUBLIC")
public class Location implements Serializable {
    @Id
    @SequenceGenerator(name = "location_seq_gen", sequenceName = "\"LOCATION_LOCATION_ID_seq\"", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_seq_gen")
    @Column(name = "\"LOCATION_ID\"")
    private Integer locationId;

    @Column(name = "\"CITY\"")
    private String city;

    @OneToMany (
            mappedBy = "location",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<CourseSession> courseSessions = new ArrayList<>();

    public Location() {
    }

    /**
     * Add session to `location`, if not already present.
     * @param session record to add to the list of session for this course session
     */
    public void addSession(CourseSession session) {
        if (this.courseSessions.stream()
                .noneMatch(courseSession ->
                        courseSession.getCourseSessionId().equals(session.getCourseSessionId())
                )
        )
            this.courseSessions.add(session);
    }

    /**
     * Remove from `location` the session indicated by `sessionId`.
     * @param sessionId used to identify the session to remove from this course
     */
    public void removeSession(Integer sessionId) {
        this.courseSessions = this.courseSessions.stream().filter(courseSession ->
                courseSession.getCourseSessionId().equals(sessionId)).collect(Collectors.toList()
        );
    }

    public Location(String city) {
        this.city = city;
    }
}
