package fr.utbm.lo54.formations.repositories;

import fr.utbm.lo54.formations.entities.Location;
import fr.utbm.lo54.formations.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class
 */
public class LocationRepository {

    private static LocationRepository instance;

    private LocationRepository() {}

    public static LocationRepository getInstance() {
        if (instance == null)
            instance = new LocationRepository();

        return instance;
    }

    /**
     * Save location into DB FORMATIONS.
     * @param location the new record to save in the DB
     * @return the newly saved location
     */
    public Location save(Location location) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            location.setLocationId((Integer) session.save(location));
            transaction.commit();
            session.close();

            return location;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Save many locations into DB FORMATIONS.
     * @param locations the list of locations to save
     * @return the newly saved list of locations
     */
    public List<Location> saveMany(List<Location> locations) {
        List<Location> savedLocations = new ArrayList<>();
        Location savedLocation = null;

        for (Location location : locations) {
            savedLocation = this.save(location);

            if (savedLocation != null)
            savedLocations.add(savedLocation);
        }
        return savedLocations;
    }

    /**
     * Get a specific location (designated by its ID) from DB FORMATIONS.
     * @param id used to identify the location to get
     * @return the location which correspond to the given ID
     */
    public Location getById(Integer id) {
        Location result = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("FROM Location LOC WHERE locationId = :id");
            query.setParameter("id", id);
            result = (Location) query.uniqueResult();

        } catch(Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Get all locations from DB FORMATIONS.
     * @return the list of all the location in the DB
     */
    public ArrayList<Location> getAll() {
        ArrayList<Location> result = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("FROM Location");
            result = new ArrayList<>(query.list());

        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Delete a specific location (designated by its ID) from DB FORMATIONS.
     * @param id used to identify the record to delete
     * @return true if succeed, false otherwise
     */
    public boolean delete(Integer id) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM Location WHERE locationId = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Update a specific location (designated by its ID) from DB FORMATIONS.
     * @param location the record to update
     * @return true if successful; false otherwise
     */
    public boolean update(Location location) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(location);
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }
}
