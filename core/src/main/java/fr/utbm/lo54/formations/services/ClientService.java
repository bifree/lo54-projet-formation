package fr.utbm.lo54.formations.services;

import fr.utbm.lo54.formations.entities.Client;
import fr.utbm.lo54.formations.repositories.ClientRepository;
import fr.utbm.lo54.formations.utils.PropertiesUtil;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Singleton class
 */
public class ClientService {

    private static ClientService instance;
    private final ClientRepository clientRepository = ClientRepository.getInstance();
    private final CourseSessionService courseSessionService = CourseSessionService.getInstance();

    private ClientService() {}

    public static ClientService getInstance() {
        if (instance == null)
            instance = new ClientService();

        return instance;
    }

    /**
     * Add `client` to DB, and return it.
     * @param client record to add to the DB
     * @return the new client with the ID from the DB
     */
    public Client createClient(Client client) {
        return this.clientRepository.save(client);
    }

    /**
     * Update `client` into DB, and return it.
     * @param client the record to update
     * @return true if successful; false otherwise
     */
    public boolean updateClient(Client client) {
        return this.clientRepository.update(client);
    }

    /**
     * Delete `client` from DB.
     * @param clientId used to identify the client to delete
     * @return true if successful; false otherwise
     */
    public boolean deleteClient(Integer clientId) {
        return this.clientRepository.delete(clientId);
    }

    /**
     * Get a client thanks to its ID.
     * @param clientId used to identify the client to get
     * @return the client which correspond to the given ID
     */
    public Client getClientById(Integer clientId) {
        return this.clientRepository.getById(clientId);
    }

    /**
     * Get all clients.
     * @return the list of all the clients in the DB
     */
    public List<Client> getAllClients() {
        return this.clientRepository.getAll();
    }

    public boolean subscribeToCourseSession(Integer clientId, Integer courseSessionId) {
        GroovyClassLoader loader = new GroovyClassLoader();
        try {
            // get the path to the groovy file
            String folder = PropertiesUtil.getProperty("groovy.folder");
            // get the class from groovy script
            Class groovy = loader.parseClass(new File(folder + "scripts/ClientScript.groovy"));
            // generate a new instance of the groovy script class
            GroovyObject groovyObj = (GroovyObject)  groovy.getDeclaredConstructor().newInstance();
            // gather the parameters to give to the script
            Object[] params = {
                    clientId,
                    courseSessionId,
                    clientRepository,
                    courseSessionService
            };
            // invoke the method subscribeToCourseSession with parameters
            return (boolean) groovyObj.invokeMethod("subscribeToCourseSession", params);
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}