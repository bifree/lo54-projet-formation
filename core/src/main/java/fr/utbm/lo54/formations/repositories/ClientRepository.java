package fr.utbm.lo54.formations.repositories;

import fr.utbm.lo54.formations.entities.Client;
import fr.utbm.lo54.formations.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class
 */
public class ClientRepository implements Serializable {

    private static ClientRepository instance;

    private ClientRepository() {}

    public static ClientRepository getInstance() {
        if (instance == null)
            instance = new ClientRepository();

        return instance;
    }

    /**
     * Save client into DB FORMATIONS.
     * @param client object to save in the DB
     */
    public Client save(Client client) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            client.setClientId((Integer) session.save(client));
            transaction.commit();
            session.close();

            return client;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Save many clients into DB FORMATIONS.
     * @param clients list of clients to save
     */
    public List<Client> saveMany(List<Client> clients) {
        List<Client> savedClients = new ArrayList<>();
        Client savedClient = null;

        for (Client client : clients) {
            savedClient = this.save(client);

            if (savedClient != null)
                savedClients.add(savedClient);
        }
        return savedClients;
    }

    /**
     * Get a specific client (designated by its ID) from DB FORMATIONS.
     * @param id used to identify the client to get
     * @return the client for the given ID
     */
    public Client getById(Integer id) {
        Client result = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("FROM Client WHERE clientId = :id");
            query.setParameter("id", id);
            result = (Client) query.uniqueResult();

        } catch(Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Get all clients from DB FORMATIONS.
     * @return the list of all the clients
     */
    public ArrayList<Client> getAll() {
        ArrayList<Client> result = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("FROM Client");
            result = new ArrayList<>(query.list());

        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Delete a specific client (designated by its ID) from DB FORMATIONS.
     * @param id used to identify the client to delete
     * @return true if successful; false otherwise
     */
    public boolean delete(Integer id) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM Client WHERE clientId = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Update a specific client (designated by its ID) from DB FORMATIONS.
     * @param client the record to update
     * @return true if successful; false otherwise
     */
    public boolean update(Client client) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.saveOrUpdate(client);
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }
}
