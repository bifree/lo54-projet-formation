package fr.utbm.lo54.formations.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "\"CLIENT\"")
public class Client implements Serializable {
    @Id
    @SequenceGenerator(name = "client_seq_gen", sequenceName = "\"CLIENT_ID_seq\"", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_seq_gen")
    @Column(name = "\"CLIENTID\"")
    private Integer clientId;

    @Column(name = "\"LASTNAME\"")
    private String lastName;

    @Column(name = "\"FIRSTNAME\"")
    private String firstName;

    @Column(name = "\"ADDRESS_CITY\"")
    private String addressCity;

    @Column(name = "\"ADDRESS_STREET\"")
    private String addressStreet;

    @Column(name = "\"PHONE\"")
    private String phone;

    @Column(name = "\"EMAIL\"")
    private String email;

    @ManyToOne
    @JoinColumn(name = "\"COURSE_SESSION_ID\"")
    private CourseSession courseSession;
    
    public Client() {
    }

    public Client(String lastName, String firstName, String addressCity, String addressStreet, String phone, String email, CourseSession courseSession) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
        this.phone = phone;
        this.email = email;
        this.courseSession = courseSession;
    }

}
