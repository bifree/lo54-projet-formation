package fr.utbm.lo54.formations.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name="\"COURSE_SESSION\"")
public class CourseSession {
    @Id
    @SequenceGenerator(name = "session_seq_gen", sequenceName = "\"COURSE_SESSION_COURSE_SESSION_ID_seq\"", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "session_seq_gen")
    @Column(name = "\"COURSE_SESSION_ID\"")
    private Integer courseSessionId;

    @Temporal(TemporalType.DATE)
    @Column(name = "\"START_DATE\"")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "\"END_DATE\"")
    private Date endDate;

    @Column(name = "\"MAX\"")
    private Integer max;

    @ManyToOne
    @JoinColumn(name = "\"COURSE_CODE\"")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "\"LOCATION_ID\"")
    private Location location;

    @OneToMany (
            mappedBy = "courseSession",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Client> clients = new ArrayList<>();

    public CourseSession() {
    }

    public CourseSession(Date startDate, Date endDate, Integer max, Course course, Location location, List<Client> clients) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.max = max;
        this.course = course;
        this.location = location;
        this.clients = clients;
    }

    public int getNumberofClients() {
        return clients.size();
    }

    public String getLocationName() {
        return location.getCity();
    }

    public String getCourseCode() { return course.getCodeCourse(); }

    /**
     * Add client to `clients`, if not already present.
     * @param client record to add to the list of client for this course session
     */
    public void addClient(Client client) {
        if (this.clients.stream().noneMatch(cli -> cli.getClientId().equals(client.getClientId())))
            this.clients.add(client);
    }

    /**
     * Remove from `clients` the client indicated by `clientId`.
     * @param clientId used to identify the client to remove from this session
     */
    public void removeClient(Integer clientId) {
        this.clients = this.clients.stream().filter(cli ->
                cli.getClientId().equals(clientId)).collect(Collectors.toList()
        );
    }

    public String toString() {
        return "CourseSession{"
                + " id=" + courseSessionId
                + ", course code=" + course.getCodeCourse()
                + ", location id=" + location.getLocationId()
                + ", start date=" + startDate
                + ", end date=" + endDate
                + ", max=" + max
                + " }";
    }
}
