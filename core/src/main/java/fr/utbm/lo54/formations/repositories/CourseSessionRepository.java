package fr.utbm.lo54.formations.repositories;

import fr.utbm.lo54.formations.entities.CourseSession;
import fr.utbm.lo54.formations.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class
 */
public class CourseSessionRepository {

    private static CourseSessionRepository instance;

    private CourseSessionRepository() {}

    public static CourseSessionRepository getInstance() {
        if (instance == null)
            instance = new CourseSessionRepository();

        return instance;
    }

    /**
     * Save one course session into DB FORMATIONS.
     * @param courseSession record to save in the DB
     * @return the newly saved course session
     */
    public CourseSession save(CourseSession courseSession) {
        Transaction transaction = null;

        try {
            final Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            courseSession.setCourseSessionId((Integer) session.save(courseSession));
            transaction.commit();
            session.close();

            return courseSession;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Save many sessions into DB FORMATIONS.
     * @param courseSessions the list of session to save in the DB
     * @return the newly saved list of course sessions
     */
    public List<CourseSession> saveMany(List<CourseSession> courseSessions) {
        List<CourseSession> savedSessions = new ArrayList<>();
        CourseSession savedSession = null;

        for (CourseSession courseSession : courseSessions) {
            savedSession = this.save(courseSession);

            if (savedSession != null)
                savedSessions.add(savedSession);
        }
        return savedSessions;
    }

    /**
     * Get a specific session (designated by its ID) from DB FORMATIONS.
     * @param id used to identify the session to get from the DB
     * @return the record which correspond to the given ID
     */
    public CourseSession getById(Integer id) {
        CourseSession result = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("FROM CourseSession WHERE courseSessionId = :id");
            query.setParameter("id", id);
            result = (CourseSession) query.uniqueResult();

        } catch(Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Get all courses sessions from DB FORMATIONS.
     * @return the list of all the course sessions
     */
    public ArrayList<CourseSession> getAll() {
        ArrayList<CourseSession> result = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("FROM CourseSession");
            result = new ArrayList<>(query.list());

        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Delete a specific session (designated by its ID) from DB FORMATIONS.
     * @param id used to identify the course session to delete
     * @return true if successful; false otherwise
     */
    public boolean delete(Integer id) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM CourseSession WHERE courseSessionId = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Update a specific session (designated by its ID) from DB FORMATIONS.
     * @param courseSession record to update
     * @return true if successful; false otherwise
     */
    public boolean update(CourseSession courseSession) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.saveOrUpdate(courseSession);
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }
}

