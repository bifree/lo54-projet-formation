package fr.utbm.lo54.formations.repositories;

import fr.utbm.lo54.formations.entities.Course;
import fr.utbm.lo54.formations.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.TemporalType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class
 */
public class CourseRepository {

    private static CourseRepository instance;

    private CourseRepository() {}

    public static CourseRepository getInstance() {
        if (instance == null)
            instance = new CourseRepository();

        return instance;
    }

    /**
     * Save course into DB FORMATIONS.
     * @param course record to save in the DB
     */
    public Course save(Course course) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            course.setCodeCourse((String) session.save(course));
            transaction.commit();
            session.close();

            return course;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Save many courses into DB FORMATI    ONS.
     * @param courses list of course to save in the DB
     */
    public List<Course> saveMany(List<Course> courses) {
        List<Course> savedCourses = new ArrayList<>();
        Course savedCourse = null;

        for (Course course : courses) {
            savedCourse = this.save(course);

            if(savedCourse != null)
                savedCourses.add(savedCourse);
        }
        return savedCourses;
    }

    /**
     * Get a specific course (designated by its ID) from DB FORMATIONS.
     * @param code used to identify the course to get
     * @return the course which correspond to the given ID
     */
    public Course getById(String code) {
        Course result = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("FROM Course WHERE codeCourse = :code");
            query.setParameter("code", code);
            result = (Course) query.uniqueResult();

        } catch(Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Get all courses from DB FORMATIONS.
     * @return the list of all the courses
     */
    public ArrayList<Course> getAll() {
        ArrayList<Course> result = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("FROM Course");
            result = new ArrayList<>(query.list());

        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get all courses from DB FORMATIONS ordered accordingly to the given parameter.
     * @param orderBy parameter indicating the ordering for the list
     * @return the list of all the courses
     */
    public ArrayList<Course> getAll(String orderBy) {
        ArrayList<Course> result = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            String hsql = "from Course crs order by crs." + orderBy + " asc";
            Query query = session.createQuery(hsql);
            result = new ArrayList<>(query.list());

        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get some courses according to the filters
     * @param filterTitle filter on the title of the courses
     * @param filterDate filter on the start date of the sessions of the courses
     * @param filterLocations filter on the location of the sessions of the courses
     */
    public ArrayList<Course> getSome(String filterTitle, String filterDate, String[] filterLocations) {

        ArrayList<String> filters = new ArrayList<>();

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();

            String hsql = "select crs from Course crs left outer join crs.courseSessions ses left outer join ses.location loc";

            if (filterTitle != "") {
                String whereTitle = "crs.title like '%" + filterTitle + "%'";
                filters.add(whereTitle);
            }
            if (filterDate != null && filterDate != "") {
                String whereDate = "ses.startDate > :date";
                filters.add(whereDate);
            }
            if (filterLocations != null) {
                String whereLocation = "loc.city in ( '" + filterLocations[0] +"'";
                for (int i = 1; i < filterLocations.length; i++) {
                    whereLocation = whereLocation.concat(", '" + filterLocations[i] +"'");
                }
                whereLocation = whereLocation.concat(" )");
                filters.add(whereLocation);
            }

            if (filters.size() > 0) {
                hsql = hsql.concat(" where ");
                for (int j = 0; j < filters.size() - 1; j++) {
                    hsql = hsql.concat(filters.get(j)).concat(" and ");
                }
                hsql = hsql.concat(filters.get(filters.size() - 1));
            }

            hsql = hsql.concat(" order by crs.codeCourse asc");

            Query query = session.createQuery(hsql);
            if (hsql.contains("ses.startDate"))
                query.setParameter("date", new SimpleDateFormat("yyyy-MM-dd").parse(filterDate), TemporalType.DATE);

            return new ArrayList<Course>(query.list());

        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Delete a specific course (designated by its ID) from DB FORMATIONS.
     * @param code used to identify the course to delete
     * @return true if successful, false otherwise
     */
    public boolean delete(String code) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM Course WHERE codeCourse = :code");
            query.setParameter("code", code);
            query.executeUpdate();
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Update a specific course (designated by its ID) from DB FORMATIONS.
     * @param course record to update
     * @return true if successful, false otherwise
     */
    public boolean update(Course course) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.saveOrUpdate(course);
            transaction.commit();
            session.close();

            return true;

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        }
    }
}

