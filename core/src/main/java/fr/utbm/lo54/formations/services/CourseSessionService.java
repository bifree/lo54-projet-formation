package fr.utbm.lo54.formations.services;

import fr.utbm.lo54.formations.entities.Client;
import fr.utbm.lo54.formations.entities.CourseSession;
import fr.utbm.lo54.formations.repositories.CourseSessionRepository;

import java.io.Serializable;
import java.util.List;

/**
 * Singleton class
 */
public class CourseSessionService implements Serializable {

    private final CourseSessionRepository courseSessionRepository = CourseSessionRepository.getInstance();
    private static CourseSessionService instance;

    private CourseSessionService() {}

    public static CourseSessionService getInstance() {
        if (instance == null)
            instance = new CourseSessionService();

        return instance;
    }

    /**
     * Add `courseSession` to DB, and return it (with ID from DB).
     * @param courseSession record to create and save in the DB
     * @return the newly created session
     */
    public CourseSession createCourseSession(CourseSession courseSession) {
        return this.courseSessionRepository.save(courseSession);
    }

    /**
     * Update `courseSession` into DB, and return it.
     * @param courseSession record to update
     * @return true if successful; false otherwise
     */
    public boolean updateCourseSession(CourseSession courseSession) {
        return this.courseSessionRepository.update(courseSession);
    }

    /**
     * Delete `courseSession` from DB.
     * @param courseSessionId used to identify the session to delete
     * @return true if succeed, false otherwise
     */
    public boolean deleteCourseSession(Integer courseSessionId) {
        return this.courseSessionRepository.delete(courseSessionId);
    }

    /**
     * Get a courseSession thanks to its ID.
     * @param courseSessionId used to identify the record to get
     * @return the session which correspond to the given ID
     */
    public CourseSession getCourseSessionById(Integer courseSessionId) {
        return this.courseSessionRepository.getById(courseSessionId);
    }

    /**
     * Get all course sessions.
     * @return the list of all the sessions in the DB
     */
    public List<CourseSession> getAllCourseSessions() {
        return this.courseSessionRepository.getAll();
    }

    /**
     * Get all clients for a given session.
     * @param courseSessionId used to identify for which session we want to get the clients
     * @return the list of clients for the given session
     */
    public List<Client> getClientsByCourseSessionId(Integer courseSessionId) {
        return this.courseSessionRepository.getById(courseSessionId).getClients();
    }

    /**
     * Add a `client` to `courseSession`, then update `courseSession`.
     * @param client the new client for the given session
     * @param courseSession the session for which a client is added
     * @return true if successful; false otherwise
     */
    public boolean addClientToCourseSession(Client client, CourseSession courseSession) {
        // add the client to courseSession
        courseSession.addClient(client);

        // update courseSession
        return courseSessionRepository.update(courseSession);
    }

    /**
     * Remove a client (identified by `clientId`) from `courseSession`, then update `courseSession`.
     * @param clientId used to identify the client to removed from the list of clients of a given session
     * @param courseSession the session from which the client is removed
     * @return true if successful; false otherwise
     */
    public boolean removeClientFromCourseSession(Integer clientId, CourseSession courseSession) {
        // remove the client from courseSession
        courseSession.removeClient(clientId);

        // update courseSession
        return courseSessionRepository.update(courseSession);
    }
}
