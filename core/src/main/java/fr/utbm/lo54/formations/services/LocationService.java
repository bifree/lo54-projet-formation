package fr.utbm.lo54.formations.services;

import fr.utbm.lo54.formations.entities.CourseSession;
import fr.utbm.lo54.formations.entities.Location;
import fr.utbm.lo54.formations.repositories.LocationRepository;

import java.util.List;

/**
 * Singleton class
 */
public class LocationService {

    private final LocationRepository locationRepository = LocationRepository.getInstance();
    private static LocationService instance;

    private LocationService() {}

    public static LocationService getInstance() {
        if (instance == null)
            instance = new LocationService();

        return instance;
    }

    /**
     * Add `location` to DB, and return it.
     * @param location record to create
     * @return the newly created location
     */
    public Location createLocation(Location location) {
        return this.locationRepository.save(location);
    }

    /**
     * Update `location` into DB, and return it.
     * @param location record to update
     * @return true if successful; false otherwise
     */
    public boolean updateLocation(Location location) {
        return this.locationRepository.update(location);
    }

    /**
     * Delete `location` from DB.
     * @param locationId used to identify the location to delete
     * @return true if successful; false otherwise
     */
    public boolean deleteLocation(Integer locationId) {
        return this.locationRepository.delete(locationId);
    }

    /**
     * Get a location thanks to its ID.
     * @param locationId used to identify the location to get
     * @return the location which correspond to the given ID
     */
    public Location getLocationById(Integer locationId) {
        return this.locationRepository.getById(locationId);
    }

    /**
     * Get all course locations.
     * @return the list of all the location in the DB
     */
    public List<Location> getAllLocations() {
        return this.locationRepository.getAll();
    }

    /**
     * Get all course sessions for a given location.
     * @param locationId used to identify for which location we want to get the sessions
     * @return the list of the sesssions for a given location
     */
    public List<CourseSession> getCourseSessionsByLocationId(Integer locationId) {
        return this.locationRepository.getById(locationId).getCourseSessions();
    }

    /**
     * Add a `session` to `location`, then update `location`.
     * @param session the new session for the given location
     * @param location the location for which a session is added
     * @return true if successful; false otherwise
     */
    public boolean addSessionToLocation(CourseSession session, Location location) {
        // add the session to location
        location.addSession(session);

        // update location
        return locationRepository.update(location);
    }

    /**
     * Remove a session (identified by `sessionId`) from `location`, then update `location`.
     * @param sessionId used to identify the session to remove from the list of sessions of a given location
     * @param location the location from which the session is removed
     * @return true if successful; false otherwise
     */
    public boolean removeSessionFromLocation(Integer sessionId, Location location) {
        // remove the session from location
        location.removeSession(sessionId);

        // update location
        return locationRepository.update(location);
    }
}
