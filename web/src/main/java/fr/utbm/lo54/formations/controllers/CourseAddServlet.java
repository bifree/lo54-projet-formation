package fr.utbm.lo54.formations.controllers;

import fr.utbm.lo54.formations.entities.Course;
import fr.utbm.lo54.formations.services.CourseService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "CourseAddServlet")
public class CourseAddServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String codeCourse = request.getParameter("code").trim();
        String title = request.getParameter("title").trim();
        Course course = new Course(codeCourse, title);

        CourseService service = CourseService.getInstance();
        Boolean success;
        Course createdCourse = service.createCourse(course);
        success = (createdCourse != null);

        HttpSession session = request.getSession();
        if (!success) session.setAttribute("error", true);

        String path = session.getAttribute("filteredList") != null
                ? "/course-filter" : "/course-list";
        response.sendRedirect(this.getServletContext().getContextPath() + path);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }
}
