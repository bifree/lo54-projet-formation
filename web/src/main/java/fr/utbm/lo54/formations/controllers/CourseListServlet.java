package fr.utbm.lo54.formations.controllers;

import fr.utbm.lo54.formations.entities.Course;
import fr.utbm.lo54.formations.entities.Location;
import fr.utbm.lo54.formations.services.CourseService;
import fr.utbm.lo54.formations.services.LocationService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CourseListServlet")
public class CourseListServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        loadLocationOnce(session);

        configureAddFunctionality(request);
        configureSearchFunctionality(request);
        defineIfPreviousError(request, session);

        Object loc = session.getAttribute("searchedLocations");
        if (loc != null) {
            List<String> loca = (List<String>) loc;
            Boolean b = loca.contains("Belfort");
        }

        loadCourses(request, session);
        this.getServletContext().getRequestDispatcher("/WEB-INF/pages/courseList.jsp").forward(request, response);
    }

    private void configureAddFunctionality(HttpServletRequest request) {
        Boolean addCourse = Boolean.parseBoolean(request.getParameter("addCourse"));
        request.setAttribute("addCourse", addCourse);
    }

    private void configureSearchFunctionality(HttpServletRequest request) {
        if (request.getAttribute("searchCourse") == null) {
            Boolean searchCourse = Boolean.parseBoolean(request.getParameter("searchCourse"));
            request.setAttribute("searchCourse", searchCourse);
        }
    }

    private void loadCourses(HttpServletRequest request, HttpSession session) {
        List<Course> filteredList = (List<Course>) session.getAttribute("filteredList");
        if (filteredList == null) {
            CourseService service = CourseService.getInstance();
            List<Course> list = service.getAllCourses("codeCourse");
            request.setAttribute("list", list);
        } else {
            request.setAttribute("list", filteredList);
        }
    }

    private void defineIfPreviousError(HttpServletRequest request, HttpSession session) {
        try {
            Boolean error = (Boolean) session.getAttribute("error");
            request.setAttribute("error", error);
            session.removeAttribute("error");

        } catch (NullPointerException e) { }
    }

    private void loadLocationOnce(HttpSession session) {
        if (session.getAttribute("locations") == null) {
            LocationService service = LocationService.getInstance();
            List<Location> locations = service.getAllLocations();
            session.setAttribute("locations", locations);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }
}
