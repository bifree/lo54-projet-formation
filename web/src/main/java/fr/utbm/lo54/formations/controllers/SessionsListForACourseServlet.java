package fr.utbm.lo54.formations.controllers;

import fr.utbm.lo54.formations.services.CourseService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "SessionsListForACourseServlet")
public class SessionsListForACourseServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String codeCourse = request.getParameter("courseId");

        CourseService service = CourseService.getInstance();
        Object courseAndSessions = service.getCourseSessionsByCourseCode(codeCourse);

        HttpSession session = request.getSession();
        if (courseAndSessions == null) {
            session.setAttribute("error", true);
        } else {
            session.setAttribute("modalContent", courseAndSessions);
        }

        response.sendRedirect(this.getServletContext().getContextPath() + "/course-list");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }
}
