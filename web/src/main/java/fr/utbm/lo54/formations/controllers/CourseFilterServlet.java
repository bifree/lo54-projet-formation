package fr.utbm.lo54.formations.controllers;

import fr.utbm.lo54.formations.entities.Course;
import fr.utbm.lo54.formations.services.CourseService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "CourseFilterServlet")
public class CourseFilterServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("searchTitle");
        String date = request.getParameter("searchDate");
        String[] locations = null;

        HttpSession session = request.getSession();
        session.setAttribute("searchedTitle", title);
        session.setAttribute("searchedDate", date);
        try {
            locations = request.getParameterValues("searchLocation");
            session.setAttribute("searchedLocations", Arrays.asList(locations));
        } catch (NullPointerException e) {
            session.setAttribute("searchedLocations", null);
        }

        processRequest(response, title, date, locations, session);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String title = (String) session.getAttribute("searchedTitle");
        String date = (String) session.getAttribute("searchedDate");
        String[] locations = (String[]) session.getAttribute("searchedLocation");

        processRequest(response, title, date, locations, session);
    }

    private void processRequest(HttpServletResponse response, String title, String date, String[] locations, HttpSession session) throws IOException {
        if (!isNull(title) || !isNull(date) || !isNull(locations)) {
            CourseService service = CourseService.getInstance();
            List<Course> list = service.getSome(title, date, locations);
            session.setAttribute("filteredList", list);
        } else {
            session.removeAttribute("filteredList");
        }

        response.sendRedirect(getServletContext().getContextPath() + "/course-list?searchCourse=" + true);
    }

    private static boolean isNull(String s) {
        return s == null || s == "";
    }

    private static boolean isNull(String[] s) {
        return s == null || s.length == 0;
    }
}
