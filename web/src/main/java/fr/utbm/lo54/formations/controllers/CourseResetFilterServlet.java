package fr.utbm.lo54.formations.controllers;

import fr.utbm.lo54.formations.entities.Course;
import fr.utbm.lo54.formations.services.CourseService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "CourseResetFilterServlet")
public class CourseResetFilterServlet extends HttpServlet {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.removeAttribute("searchedTitle");
        session.removeAttribute("searchedDate");
        session.removeAttribute("searchedLocations");
        session.removeAttribute("filteredList");

        response.sendRedirect(getServletContext().getContextPath() + "/course-list?searchCourse=" + true);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
