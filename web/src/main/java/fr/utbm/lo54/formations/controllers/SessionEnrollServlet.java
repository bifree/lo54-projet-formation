package fr.utbm.lo54.formations.controllers;

import fr.utbm.lo54.formations.entities.Client;
import fr.utbm.lo54.formations.entities.CourseSession;
import fr.utbm.lo54.formations.services.ClientService;
import fr.utbm.lo54.formations.services.CourseSessionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "SessionEnrollServlet")
public class SessionEnrollServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String courseCode = "";

        Client client = new Client();
        setClientAccordingToForm(request, client);

        ClientService service = ClientService.getInstance();
        Boolean success;

        Client createdClient = service.createClient(client);
        success = (createdClient != null);

        if (success) {
            Integer sessionId = Integer.parseInt(request.getParameter("sessionId").trim());
            success = service.subscribeToCourseSession(createdClient.getClientId(), sessionId);

            CourseSessionService sessionService = CourseSessionService.getInstance();
            CourseSession courseSession = sessionService.getCourseSessionById(sessionId);

            if (courseSession != null) {
                courseCode = courseSession.getCourseCode();
            } else {
                success = false;
            }
        }

        if (!success) session.setAttribute("error", true);

        String path = success ? "/load-sessions?courseId=" + courseCode : "/course-list";
        response.sendRedirect(this.getServletContext().getContextPath() + path);
    }

    private void setClientAccordingToForm(HttpServletRequest request, Client client) {
        client.setFirstName(request.getParameter("firstname").trim());
        client.setLastName(request.getParameter("name").trim());
        client.setAddressCity(request.getParameter("city").trim());
        client.setAddressStreet(request.getParameter("street").trim());
        client.setPhone(request.getParameter("tel").trim());
        client.setEmail(request.getParameter("email").trim());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }
}
