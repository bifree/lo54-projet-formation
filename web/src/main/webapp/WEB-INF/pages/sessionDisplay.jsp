<tr>
    <td><fmt:formatDate value="${ session.getStartDate() }" pattern="dd/MM/yyyy"/></td>
    <td><fmt:formatDate value="${ session.getEndDate() }" pattern="dd/MM/yyyy"/></td>
    <td><c:out value="${ session.getLocationName() }"/></td>
    <td>
        <div class="progress">
        <c:choose>
            <c:when test="${ session.getNumberofClients() == 0 }">
                <div class="progress-bar" role="progressbar"
                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="${ session.getMax() }">
                </div>
            </c:when>
            <c:when test="${ session.getNumberofClients() == session.getMax()}">
                <div class="progress-bar bg-danger" role="progressbar"
                     style="width: 100%" aria-valuenow="${ session.getNumberofClients()}"
                     aria-valuemin="0" aria-valuemax="${ session.getMax() }">
                     <c:out value="${ session.getNumberofClients() }"/>/<c:out value="${ session.getMax() }"/>
                </div>
            </c:when>
            <c:otherwise>
                <div class="progress-bar" role="progressbar"
                     style="width: ${session.getNumberofClients() * 100 / session.getMax()}%" aria-valuenow="${ session.getNumberofClients()}"
                     aria-valuemin="0" aria-valuemax="${ session.getMax() }">
                     <c:out value="${ session.getNumberofClients() }"/>/<c:out value="${ session.getMax() }"/>
                </div>
            </c:otherwise>
        </c:choose>
        </div>
    </td>
    <td>
        <c:if test="${ (now lt session.getEndDate()) && (session.getNumberofClients() != session.getMax()) }">
            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#enrollModal" data-sessionId="${ session.getCourseSessionId() }">
                <i class="fas fa-sign-in-alt"></i>
            </button>
        </c:if>
    </td>
</tr>
