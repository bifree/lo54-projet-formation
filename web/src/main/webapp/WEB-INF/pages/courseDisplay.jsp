<tr>
    <td><c:out value="${ course.getCodeCourse() }"/></td>
    <td>
        <c:out value="${ course.getTitle() }"/>
    </td>
    <td class="actions">
        <c:if test="${ course.getNumberOfSessions() != 0 }">
            <a href="load-sessions?courseId=${ course.getCodeCourse() }"
                    type="button" class="btn btn-primary existingSessionsButton">
                <small class="existingSessions">
                    <c:out value="${ course.getNumberOfSessions() }"/> session(s)
                </small>
                <i class="far fa-eye"></i>
            </a>
        </c:if>
        <a href="course-edit?courseId=${ course.getCodeCourse() }" type="button" class="btn btn-success">
            <i class="fas fa-edit"></i>
        </a>
        <a href="course-delete?courseId=${ course.getCodeCourse() }" type="button" class="btn btn-danger">
            <i class="far fa-trash-alt"></i>
        </a>
    </td>
</tr>
