<!-- Modal -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<div class="modal fade" id="enrollModal" tabindex="-1" role="dialog" aria-labelledby="enrollModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="enrollModalLabel">Inscription</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form accept-charset="ISO-8859-1" method="post" action="enroll">
                    <div class="row">
                        <input type="hidden" name="sessionId" value=""/>
                        <div class="form-group col">
                            <label class="text-muted"><small>Pr�nom</small>
                                <input type="text" name="firstname" class="form-control"
                                       minlength="3" maxlength="20"
                                       pattern=".*\S+.*" placeholder="John" required>
                            </label>
                        </div>
                        <div class="form-group col">
                            <label class="text-muted"><small>Nom</small>
                                <input type="text" name="name" class="form-control"
                                       minlength="3" maxlength="20"
                                       pattern=".*\S+.*" placeholder="Doe" required>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label class="text-muted"><small>Ville</small>
                                <input type="text" name="city" class="form-control"
                                       minlength="3" maxlength="20"
                                       pattern=".*\S+.*" placeholder="Paris" required>
                            </label>
                        </div>
                        <div class="form-group col">
                            <label class="text-muted"><small>Rue</small>
                                <input type="text" name="street" class="form-control"
                                       minlength="3" maxlength="20"
                                       pattern=".*\S+.*" placeholder="Rue des Joncquilles" required>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label class="text-muted"><small>T�l�phone</small>
                                <input type="tel" name="tel" placeholder="0601234567" class="form-control" required>
                            </label>
                        </div>
                        <div class="form-group col">
                            <label class="text-muted"><small>Email</small>
                                <input type="email" name="email" placeholder="prenom.nom@exemple.fr" class="form-control" required>
                            </label>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <button type="button" class="btn btn-outine-secondary" data-dismiss="modal">Retour</button>
                        <button type="submit" class="btn btn-primary">S'inscrire</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
