<tr>
    <form accept-charset="ISO-8859-1" method="post" action="course-update">

        <td><c:out value="${ course.getCodeCourse() }"/></td>
        <td class="input-edit">
            <input type="text" name="editTitle" class="form-control"
                   value="${ course.getTitle() }" placeholder="Intitul� de la formation">
        </td>
        <td class="actions">
            <c:if test="${ course.getNumberOfSessions() != 0 }">
                <a href="load-sessions?courseId=${ course.getCodeCourse() }"
                   type="button" class="btn btn-primary existingSessionsButton">
                    <small class="existingSessions">
                        <c:out value="${ course.getNumberOfSessions() }"/> session(s)
                    </small>
                    <i class="far fa-eye"></i>
                </a>
            </c:if>
            <button type="submit" class="btn btn-outline-success">
                <i class="fas fa-save"></i>
            </button>
            <a href="course-delete?courseId=${ course.getCodeCourse() }" type="button" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
            </a>
        </td>
    </form>
</tr>
