<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<c:if test="${not empty error}">
    <c:if test="${error}">
        <div class="alert alert-danger" role="alert">
            Un probl�me est survenu lors de l'op�ration.
        </div>
    </c:if>
</c:if>