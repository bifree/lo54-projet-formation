<!-- Modal -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<div class="modal fade" id="sessionsModal" tabindex="-1" role="dialog" aria-labelledby="sessionsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="sessionsModalLabel">
                    <c:out value="${ sessionScope.modalContent.get('course').getCodeCourse() }"/>
                     -
                    <c:out value="${ sessionScope.modalContent.get('course').getTitle() }"/>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body sessions">
                <table class="table sortable">
                    <thead>
                        <tr>
                            <th scope="col" class="modal-title">
                                Date de d�but
                            </th>
                            <th scope="col" class="modal-title">
                                Date de fin
                            </th>
                            <th scope="col" class="modal-title">
                                Localisation
                            </th>
                            <th scope="col" class="modal-title sorttable_nosort">
                                Capacit�
                            </th>
                            <th scope="col" class="modal-title sorttable_nosort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <jsp:useBean id="now" class="java.util.Date"/>
                        <c:forEach items="${ sessionScope.modalContent.get('sessions') }" var="session">
                            <%@ include file="/WEB-INF/pages/sessionDisplay.jsp" %>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>