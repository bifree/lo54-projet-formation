<tr>
    <td colspan="3">
        <form accept-charset="ISO-8859-1" method="post" action="course-filter">
            <a href="course-reset-filter" type="button" class="btn btn-outline-danger reset-button">
                <i class="fas fa-undo reset-text"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <input type="search" id="searchTitle" name="searchTitle" class="form-control"
                               value="${ sessionScope.searchedTitle }"
                               placeholder="Rechercher selon l'intitul�">
                    </div>
                    <div class="col">
                        <input type="date" id="searchDate" name="searchDate" class="form-control"
                               value="${ sessionScope.searchedDate }"
                               placeholder="A partir de...">
                    </div>
                    <div class="col">
                        <select title="Filtrer par lieu" id="searchLocation" name="searchLocation"
                                class="selectpicker form-control" multiple>
                            <c:forEach items="${ sessionScope.locations }" var="location">
                                <option value="${ location.getCity() }" ${ sessionScope.searchedLocations.contains(location.getCity()) ? 'selected' : ' '}>
                                    <c:out value="${ location.getCity() }"/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="search">
                <button type="submit" class="btn btn-primary btn-block">
                    Filtrer
                </button>
            </div>
        </form>
    </td>
</tr>