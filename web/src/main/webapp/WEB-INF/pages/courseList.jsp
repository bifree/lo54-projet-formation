<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <%@ include file="/WEB-INF/common-parts/meta.jsp" %>
    <style><%@ include file="/resources/css/course-list.css" %></style>
    <style><%@ include file="/resources/css/course-search.css" %></style>
    <style><%@ include file="/resources/css/course-edit.css" %></style>
    <style><%@ include file="/resources/css/session-list.css" %></style>
    <style><%@ include file="/resources/css/table.css" %></style>
    <style><%@ include file="/resources/css/form.css" %></style>
</head>
<body>
<%@ include file="/WEB-INF/common-parts/header.jsp" %>
<%@ include file="/WEB-INF/pages/errorAlert.jsp" %>
<table id="table" class="table table-hover sortable">
    <thead class="thead-light">
    <tr>
        <th scope="col" class="table-title code">
            UV
        </th>
        <th scope="col" class="table-title">
            Intitul�
        </th>
        <th scope="col" class="main-actions sorttable_nosort">
            <span class="table-action">
                <a onclick="toggleSearchCourse()" class="text-primary">
                    <i class="fas fa-search fa-2x" aria-hidden="true"></i>
                </a>
            </span>
            <span class="table-action">
                <a onclick="toggleAddCourse()" class="text-success">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span>
        </th>
    </tr>
    </thead>
    <thead>
    <c:if test="${addCourse}">
        <%@ include file="/WEB-INF/pages/courseAdd.jsp" %>
    </c:if>
    <c:if test="${searchCourse}">
        <%@ include file="/WEB-INF/pages/courseSearch.jsp" %>
    </c:if>
    </thead>
    <tbody>
    <c:forEach items="${ list }" var="course">
        <c:choose>
            <c:when test="${ sessionScope.editing == course.getCodeCourse() }">
                <%@ include file="/WEB-INF/pages/courseEdit.jsp" %>
            </c:when>
            <c:otherwise>
                <%@ include file="/WEB-INF/pages/courseDisplay.jsp" %>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:if test="${ list.size() == 0 }">
        <tr>
            <td colspan="3">
                Aucune formation disponible
            </td>
        </tr>
    </c:if>
    </tbody>
</table>
<br/>
<div id="modal" data-modalContent="${ sessionScope.modalContent }"></div>
<%@ include file="/WEB-INF/pages/sessionsListModal.jsp" %>
<%@ include file="/WEB-INF/pages/enrollModal.jsp" %>
<%@ include file="/WEB-INF/common-parts/footer.jsp" %>
<script src="resources/js/sorttable.js"></script>
<script type="text/javascript">
    $(document).ready(openModal());

    function openModal() {
        const modalContent = document.getElementById("modal").getAttribute("data-modalContent");

        if (modalContent !== "") {
            $('#sessionsModal').modal("show");
        }
    }

    $('#sessionsModal').on('hide.bs.modal', function () {
        location.href = "reset-sessions-list";
    })

    $('#enrollModal').on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget); // Button that triggered the modal
        const sessionId = button.attr('data-sessionId'); // Extract info from data-* attributes

        console.log(button, sessionId);
        $('input[name="sessionId"]').val(sessionId);
        const modal = $(this);
        modal.find('.modal-body input.inputSession').val(sessionId);
    })

    function toggleAddCourse() {
        const add = !${addCourse};
        const options = "course-list?"
            + "addCourse=" + add
        ;
        location.href = options;
    }

    function toggleSearchCourse() {
        const search = !${searchCourse};
        const options = "course-list?"
            + "searchCourse=" + search
        ;
        location.href = options;
    }
</script>
</body>
</html>