<tr>
    <form accept-charset="ISO-8859-1" method="post" action="course-add">
        <td>
            <input type="text" name="code" class="form-control"
                   minlength="3" maxlength="5"
                   pattern=".*\S+.*" title="Code non null (ex: EV00)"
                   placeholder="Code de l'UV" required>
        </td>
        <td>
            <input type="text" name="title" class="form-control"
                   minlength="5" maxlength="60"
                   pattern=".*\S+.*" title="Intitul� non null donnant une description explicite (ex: Initiation � l'environnement et � ses probl�matiques)"
                   placeholder="Intitul� de l'UV" required>
        </td>
        <td>
            <button type="submit" class="btn btn-success btn-block">
                Ajouter le cours
            </button>
        </td>
    </form>
</tr>