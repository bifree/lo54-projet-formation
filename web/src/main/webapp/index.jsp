<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%@ include file="/WEB-INF/common-parts/meta.jsp" %>
</head>
<body>
    <%@ include file="/WEB-INF/common-parts/header.jsp" %>
    <jsp:forward page="/course-list" />
    <%@ include file="/WEB-INF/common-parts/footer.jsp" %>
</body>
</html>
